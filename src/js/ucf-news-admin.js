!(function (e) {
  e(".section-input").suggest(ajaxurl + "?action=ucf-news-sections", { delay: 500, minchars: 2, multiple: !0 }), e(".topic-input").suggest(ajaxurl + "?action=ucf-news-topics", { delay: 500, minchars: 2, multiple: !0 });
})(jQuery),
  (function (l) {
      l(".ucf_news_fallback_image_upload").click(function (e) {
          e.preventDefault();
          var a = wp
              .media({ title: "News Fallback Image", button: { text: "Select Image" }, multiple: !1 })
              .on("select", function () {
                  var e = a.state().get("selection").first().toJSON();
                  l(".ucf_news_fallback_image_preview").attr("src", e.url),
                      l(".ucf_news_fallback_image_upload").text("Change Image"),
                      l(".ucf_news_fallback_image").val(e.id),
                      l(".ucf_news_fallback_image_preview").show(),
                      l(".ucf_news_clear_image_upload").show(),
                      l(".ucf_news_fallback_message").hide();
              })
              .open();
      }),
          l(".ucf_news_clear_image_upload ").click(function (e) {
              e.preventDefault(),
                  l(".ucf_news_fallback_image_preview").hide(),
                  l(".ucf_news_fallback_image_preview").attr("src", ""),
                  l(".ucf_news_fallback_image_upload").text("Add Image"),
                  l(".ucf_news_clear_image_upload").hide(),
                  l(".ucf_news_fallback_message").show(),
                  l(".ucf_news_fallback_image").val("");
          });
  })(jQuery);
