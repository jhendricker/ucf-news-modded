<?php
/**
 * The default functions for the featured card home layout
 **/
if ( ! function_exists( 'ucf_news_display_featured_card_home_before' ) ) {
	function ucf_news_display_featured_card_home_before( $content, $items, $args, $display_type ) {
		ob_start();
	?>
		<div class="ucf-news modern featured-card-home">
	<?php
		return ob_get_clean();
	}

	add_filter( 'ucf_news_display_featured_card_home_before', 'ucf_news_display_featured_card_home_before', 10, 4 );
}

if ( ! function_exists( 'ucf_news_display_featured_card_home_title' ) ) {
	function ucf_news_display_featured_card_home_title( $content, $items, $args, $display_type ) {
		$formatted_title = $args['title'];

		switch( $display_type ) {
			case 'widget':
				break;
			case 'default':
			default:
				if ( $formatted_title ) {
					$formatted_title = '<h3 class="ucf-news-title h6 heading-underline mb-0" style="font-weight:bold;">' . $formatted_title . '</h3>';
				}
				break;
		}

		return $formatted_title;
	}

	add_filter( 'ucf_news_display_featured_card_home_title', 'ucf_news_display_featured_card_home_title', 10, 4 );
}

if ( ! function_exists( 'ucf_news_display_featured_card_home' ) ) {
	function ucf_news_display_featured_card_home( $content, $items, $args, $display_type, $fallback_message ) {
		if ( ! is_array( $items ) ) { $items = array( $items ); }

		ob_start();

	if ( count( $items ) === 0 ) :
		echo $fallback_message; 

	elseif( count( $items ) === 1 ) :

		foreach( $items as $item ) :
			$item_img = UCF_News_Common::get_story_image_or_fallback( $item, $args );
	?>
	<div class="ucf-news-item col-sm-12">
		<a href="<?php echo $item->link; ?>">
		<?php if ( isset( $item_img['img_url'] ) ) : ?>
			<div class="mb-3">
			<img src="<?php echo $item_img['img_url']; ?>" class="ucf-news-thumbnail-image" alt="<?php echo (isset($item_img['img_alt']) ? $item_img['img_alt']:'');?>">
			</div>
		<?php endif; ?>
			<div class="ucf-news-item-content">
				<div class="ucf-news-item-details">
					<p class="ucf-news-item-title" style="font-weight:bold;"><?php echo $item->title->rendered; ?></p>
					<p class="ucf-news-item-excerpt mb-3"><?php echo wp_trim_words( $item->excerpt->rendered, 25 ); ?></p>
					<a class="btn btn-primary mb-0" href="<?php echo $item->link; ?>">Read More</a>
				</div>
			</div>
		</a>
	</div>
	<?php endforeach;
	
	else : 

		foreach( $items as $item ) :
			$item_img = UCF_News_Common::get_story_image_or_fallback( $item, $args );
			$sections = UCF_News_Common::get_story_sections( $item );
			$section = $sections[0];
	?>
	<div class="ucf-news-item col-sm-12">
		<a href="<?php echo $item->link; ?>">
		<?php if ( isset( $item_img['img_url'] ) ) : ?>
			<div class="mb-3">
			<img src="<?php echo $item_img['img_url']; ?>" class="ucf-news-thumbnail-image" alt="<?php echo (isset($item_img['img_alt']) ? $item_img['img_alt']:'');?>">
			</div>
		<?php endif; ?>
			<div class="ucf-news-item-content">
				<div class="ucf-news-item-details">
					<p class="ucf-news-item-title" style="font-weight:bold;"><?php echo $item->title->rendered; ?></p>
					<p class="ucf-news-item-excerpt mb-3"><?php echo wp_trim_words( $item->excerpt->rendered, 25 ); ?></p>
					<a class="btn btn-primary mb-0" href="<?php echo $item->link; ?>">Read More</a>
				</div>
			</div>
		</a>
	</div>
	<?php
		endforeach;

	endif; // End if item count

		return ob_get_clean();
	}

	add_filter( 'ucf_news_display_featured_card_home', 'ucf_news_display_featured_card_home', 10, 5 );
}

if ( ! function_exists( 'ucf_news_display_featured_card_home_after' ) ) {
	function ucf_news_display_featured_card_home_after( $content, $items, $args, $display_type ) {
		ob_start();
	?>
		</div>
	<?php
		return ob_get_clean();
	}

	add_filter( 'ucf_news_display_featured_card_home_after', 'ucf_news_display_featured_card_home_after', 10, 4 );
}
