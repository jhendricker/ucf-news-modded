<?php
/*
Plugin Name: UCF News Modded
Description: Contains shortcode and widget for displaying COS News Feeds or other feeds with categories and tags.
Version: 2.1.9
Author: UCF Web Communications / UCF College of Sciences
License: GPL3
Bitbucket Plugin URI: jhendricker/ucf-news-modded
*/
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action( 'plugins_loaded', function() {

	define( 'UCF_NEWS__PLUGIN_FILE', __FILE__ );

	require_once 'includes/ucf-news-config.php';
	require_once 'includes/ucf-news-feed.php';
	require_once 'includes/ucf-news-common.php';
	require_once 'includes/ucf-news-shortcode.php';
	require_once 'includes/ucf-news-widget.php';
	require_once 'admin/ucf-news-admin.php';
	require_once 'admin/ucf-news-api.php';

	require_once 'layouts/ucf-news-classic.php';
	require_once 'layouts/ucf-news-modern.php';
	require_once 'layouts/ucf-news-modern-card.php';
	require_once 'layouts/ucf-news-card.php';
	require_once 'layouts/ucf-news-featured-card-home.php';	

	add_action( 'init', array( 'UCF_News_Shortcode', 'register_shortcode' ) );
	add_action( 'admin_init', array( 'UCF_News_Shortcode', 'register_shortcode_interface' ) );
	add_action( 'admin_menu', array( 'UCF_News_Config', 'add_options_page' ) );

} );

?>
