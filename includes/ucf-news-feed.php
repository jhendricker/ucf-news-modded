<?php
/**
 * Handles all feed related code.
 **/

if ( ! class_exists( 'UCF_News_Feed' ) ) {
	class UCF_News_Feed {
		public static function get_json_feed( $feed_url ) {
			$response = wp_remote_get( $feed_url, array( 'timeout' => 15 ) );

			if ( is_array( $response ) && wp_remote_retrieve_response_code( $response ) == 200 ) {
				$result = json_decode( wp_remote_retrieve_body( $response ) );
			}
			else {
				$result = false;
			}

			return $result;
		}

		public static function format_tax_arg( $terms, $tax ) {
			$terms_filtered = is_array( $terms ) ? array_filter( $terms ) : null;

			return $terms_filtered;
		}

		public static function non_empty_allow_zero( $arg ) {
			return !(
				is_array( $arg ) && empty( $arg )
				|| is_null( $arg )
				|| is_string( $arg ) && empty( $arg )
			);
		}

		public static function get_news_items( $args ) {
			
			$url_option = get_option( 'ucf_news_feed_url' );

			$args = array(
				'url'        => $url_option ? $url_option : 'https://today.ucf.edu/wp-json/wp/v2/',
				'limit'      => isset( $args['limit'] ) ? (int) $args['limit'] : 3,
				'offset'     => isset( $args['offset'] ) ? (int) $args['offset'] : 0,
				'categories' => isset( $args['sections'] ) ? explode( ',', $args['sections'] ) : null,
				'exclude_categories' => isset( $args['exclude_sections'] ) ? explode( ',', $args['exclude_sections'] ) : null,
				'tags'       => isset( $args['topics'] ) ? explode( ',', $args['topics'] ) : null,
			);
			
			// Empty array of indexes with no value.
			$args = array_filter( $args, array( 'UCF_News_Feed', 'non_empty_allow_zero' ) );

			// Set up query params.
			$categories = $exclude_categories = $tags = array();

			if ( isset( $args['categories'] ) ) {
				$categories = self::format_tax_arg( $args['categories'], 'categories' );
			}
			if ( isset( $args['exclude_categories'] ) ) {
				$exclude_categories = self::format_tax_arg( $args['exclude_categories'], 'categories' );
			}
			if ( isset( $args['tags'] ) ) {
				$tags = self::format_tax_arg( $args['tags'], 'tag_slugs' );
			}						
			
			// Get category results by looking up category id based on the slug provided
			$catID = array();
			$category_ids  = "";			

			foreach( $categories as $category ) {
				$results = UCF_News_Feed::get_sections($category);
				if ( is_array( $results ) && !empty($results) ) {
					$cat_obj = $results[0];
				
					if ( property_exists( $cat_obj, 'id' ) ){
						$catID[] = $cat_obj->id;
					}
				}
			}
			$category_ids = implode(",", $catID);			

			// Get category results to exclude by looking up category id based on the slug provided
			$exclude_catID = array();
			$exclude_category_ids  = "";

			foreach( $exclude_categories as $exclude_category ) {
				$results = UCF_News_Feed::get_sections($exclude_category);
				if ( is_array( $results ) && !empty($results) ) {
					$exclude_cat_obj = $results[0];
				
					if ( property_exists( $exclude_cat_obj, 'id' ) ){
						$exclude_catID[] = $exclude_cat_obj->id;
					}
				}
			}
			$exclude_category_ids = implode(",", $exclude_catID);


			// Lookup tag results based on the slug provided. It will return tag info if the provided slug is part of another tag
			$tagID = array();
			$tag_ids  = "";

			foreach( $tags as $tag ) {
				$results = UCF_News_Feed::get_topics($tag);
				if ( is_array( $results ) && !empty($results) ) {
					foreach( $results as $result ){
						$tag_obj = $result;
				
						if ( property_exists( $tag_obj, 'id' ) ){
							$tagID[] = $tag_obj->id;
						}
					}
				}
			}
			$tag_ids = implode(",", $tagID);

			// If there's only category info
			if ( !empty($category_ids) && empty($tag_ids) && empty($exclude_category_ids) )
			$query = http_build_query( array(
				'per_page'		=> $args['limit'],
				'offset'		=> $args['offset'],
				'categories[terms]'	=> $category_ids,	
				'categories[operator]' => 'AND',			
				'_embed'		=> true
			) );
			// If there's only tag info
			elseif ( empty($category_ids) && !empty($tag_ids) && empty($exclude_category_ids) )
			$query = http_build_query( array(
				'per_page'		=> $args['limit'],
				'offset'		=> $args['offset'],			
				'tags'			=> $tag_ids,
				'_embed'		=> true
			) );
			// If there is category and categories to exclude
			elseif ( !empty($category_ids) && !empty($exclude_category_ids) )
			$query = http_build_query( array(
				'per_page'		=> $args['limit'],
				'offset'		=> $args['offset'],			
				'categories[terms]'	=> $category_ids,	
				'categories[operator]' => 'AND',		
				'categories_exclude' => $exclude_category_ids,
				'_embed'		=> true
			) );
			// If there is category and tag info
			elseif ( !empty($category_ids) && !empty($tag_ids) )
			$query = http_build_query( array(
				'per_page'		=> $args['limit'],
				'offset'		=> $args['offset'],			
				'categories[terms]'	=> $category_ids,	
				'categories[operator]' => 'AND',		
				'tags'			=> $tag_ids,
				'_embed'		=> true
			) );
			else 
				return null;							
			
			//$query = preg_replace( '/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $query );
			$query = preg_replace( '/%5B[0-9]+%5D/simU', '%5B%5D', $query );
			$query = str_replace( '%5B', '[', $query );
			$query = str_replace( '%5D', ']', $query );
			$query = str_replace( '%2C', ',', $query );

			// Fetch feed
			$feed_url = $args['url'] . 'posts?' . $query;

			return self::get_json_feed( $feed_url );
		}

		public static function get_sections( $search ) {
			$base_url = get_option( 'ucf_news_feed_url', 'https://today.ucf.edu/wp-json/wp/v2/' );
			$url      = $base_url . 'categories/?search=' . $search;

			return self::get_json_feed( $url );
		}

		public static function get_topics( $search ) {
			$base_url = get_option( 'ucf_news_feed_url', 'https://today.ucf.edu/wp-json/wp/v2/' );
			$url      = $base_url . 'tags/?search=' . $search;

			return self::get_json_feed( $url );
		}
	}
}
?>
