<?php
/**
 * Handles the registration of the UCF News Shortcode
 **/

if ( ! class_exists( 'UCF_News_Shortcode' ) ) {
	class UCF_News_Shortcode {
		public static function register_shortcode() {
			add_shortcode( 'ucf-news-feed', array( 'UCF_News_Shortcode', 'sc_ucf_news_feed' ) );
		}

		public static function register_shortcode_interface() {
			if ( class_exists( 'UCF_Shortcode_Interface' ) ) {
				$params = array(
					array(
						'name'      => 'Title',
						'id'        => 'title',
						'help_text' => 'The title to display before the news feed',
						'type'      => 'text',
						'default'   => 'News'
					),
					array(
						'name'      => 'Layout',
						'id'        => 'layout',
						'help_text' => 'The layout to use to display the news items',
						'type'      => 'dropdown',
						'choices'   => $layouts,
						'default'   => 'modern'
					),
					array(
						'name'      => 'Filter by Section ID',
						'id'        => 'sections',
						'help_text' => 'The section id of each section to filter by',
						'type'      => 'text',
						'default'   => ''
					),
					array(
						'name'      => 'Filter by excluding Section ID',
						'id'        => 'exclude',
						'help_text' => 'The section id of each section to filter by',
						'type'      => 'text',
						'default'   => ''
					),
					array(
						'name'      => 'Filter by Topic ID',
						'id'        => 'topics',
						'help_text' => 'The topic id of each topic to filter by',
						'type'      => 'text',
						'default'   => ''
					),
					array(
						'name'      => 'Number of News Items',
						'id'        => 'limit',
						'help_text' => 'The number of news items to show',
						'type'      => 'number',
						'default'   => '3'
					),
					array(
						'name'      => 'Number of News Items Per Row',
						'id'        => 'per_row',
						'help_text' => 'The number of news items to show per row (for card layout only)',
						'type'      => 'number',
						'default'   => '3'
					),
					array(
						'name'      => 'Image Size',
						'id'        => 'image_size',
						'help_text' => 'The pre-defined image size to use (thumbnail, post_thumbnail, medium, medium_large, large, full)',
						'type'      => 'text',
						'default'   => 'thumbnail'
					)
				);

				$args = array(
					'name'        => 'UCF News Feed',
					'command'     => 'ucf-news-feed',
					'description' => 'Creates a feed of UCF News Items',
					'params'      => $params
				);

				UCF_Shrotcode_Interface::add_shortcode( $args );
			}
		}

		public static function sc_ucf_news_feed( $attr, $content='' ) {
			$attr = shortcode_atts( array(
				'title'     => 'News',
				'layout'    => 'modern',
				'sections'  => '',
				'exclude' 	=> '',
				'topics'    => '',
				'offset'    => 0,
				'limit'     => 3,
				'per_row'   => 3,
				'image_size' => 'thumbnail'
			), $attr );

			$title = $attr['title'];
			$layout = $attr['layout'];
			$per_row = $attr['per_row'];

			$args = array(
				'sections' 			=> $attr['sections'] ?: null,
				'exclude_sections' 	=> $attr['exclude'] ?: null,
				'topics'   			=> $attr['topics'] ?: null,
				'offset'   			=> $attr['offset'] ? (int) $attr['offset'] : 0,
				'limit'    			=> $attr['limit'] ? (int) $attr['limit'] : 3,
				'image_size' 		=> $attr['image_size'] ?: 'thumbnail'
			);

			$items = UCF_News_Feed::get_news_items( $args );

			ob_start();

			if ( $items !== null ) {
				echo UCF_News_Common::display_news_items( $items, $layout, array_merge( $attr, $args ), 'default', $content );
			}
			else
				echo "There are no news stories at this time.";

			return ob_get_clean();
		}
	}
}

?>
